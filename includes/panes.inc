<?php

/**
 * Provides a form element to set pane colors.
 *
 * @param string $element_name
 *   The name of the form element.
 *
 * @return array
 *   Form element array.
 */
function coco_panels_pane_colors_form_element($element_name) {
  $form_colors = coco_config_color_palette();
  $header = array(
    'name' => 'Type',
    'bg' => 'Background color',
    'text' => 'Text color',
  );
  $color_options = array(
    'none' => array(
      'name' => 'No colors',
      'bg' => '---',
      'text' => '---',
    ),
  );
  $color_css = '';
  foreach ($form_colors as $color => $options) {
    $color_options[$color] = array(
      'name' => ucfirst($color) . ' colors',
      'bg' => $options['bg'],
      'text' => $options['text'],
      '#attributes' => array('class' => array('edit-colors-row-' . $color))
    );
    $color_css .= '.edit-' . $element_name . '-row-' . $color . ' > td { text-align: left; ';
    $color_css .= ' background-color: ' . $options['bg'] . ';';
    $color_css .= ' color: ' . $options['text'] . ';';
    $color_css .= '} .edit-' . $element_name . '-row-' . $color . ' > td:first-child { width: 1em; } ';
  }

  return array(
    '#type' => 'tableselect',
    '#title' => t('Pane background and text colors'),
    '#options' => $color_options,
    '#header' => $header,
    '#default_value' => $conf['colors'],
    '#multiple' => FALSE,
    '#prefix' => '<style> ' . $color_css . '</style>',
  );
}
