<?php
/**
 * @file
 * Template for the Coco Stacked Default layout.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<?php if ($content['topheader'] || $content['topcolumn1'] || $content['topcolumn2']) : ?>
  <div class="container-fluid coco-stacked--region__top-default">
    <?php if ($content['topheader']) : ?>
      <div class="coco-stacked--header__default">
        <?php print $content['topheader']; ?>
      </div>
    <?php endif; ?>
    <?php if ($content['topcolumn1'] || $content['topcolumn2']) : ?>
      <div class="coco-stacked--columns">
        <div class="coco-stacked--column1">
          <?php print $content['topcolumn1']; ?>
        </div>
        <div class="coco-stacked--column2">
          <?php print $content['topcolumn2']; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
<?php if ($content['middleheader'] || $content['middlecolumn1'] || $content['middlecolumn2']) : ?>
  <div class="container-fluid coco-stacked--region__middle-default">
    <?php if ($content['middleheader']) : ?>
      <div class="coco-stacked--header">
        <?php print $content['middleheader']; ?>
      </div>
    <?php endif; ?>
    <?php if ($content['middlecolumn1'] || $content['middlecolumn2']) : ?>
      <div class="coco-stacked--columns">
        <div class="coco-stacked--column1">
          <?php print $content['middlecolumn1']; ?>
        </div>
        <div class="coco-stacked--column2">
          <?php print $content['middlecolumn2']; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
<?php if ($content['bottomheader'] || $content['bottomcolumn1'] || $content['bottomcolumn2']) : ?>
  <div class="container-fluid coco-stacked--region__bottom-default">
    <?php if ($content['bottomheader']) : ?>
      <div class="coco-stacked--header">
        <?php print $content['bottomheader']; ?>
      </div>
    <?php endif; ?>
    <?php if ($content['bottomcolumn1'] || $content['bottomcolumn2']) : ?>
      <div class="coco-stacked--columns">
        <div class="coco-stacked--column1">
          <?php print $content['bottomcolumn1']; ?>
        </div>
        <div class="coco-stacked--column2">
          <?php print $content['bottomcolumn2']; ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
