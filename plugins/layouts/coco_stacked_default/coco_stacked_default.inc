<?php

/**
 * @file
 * Panels layout plugin definition: Coco Stacked Default.
 *
 * Forked from the Radix 'Sutro Double' layout.
 * @see https://drupal.org/project/radix_layouts.
 */

$plugin = array(
  'title' => t('Coco Stacked Default'),
  'icon' => 'coco-stacked-default.png',
  'category' => t('CocoCore'),
  'theme' => 'coco_stacked_default',
  'regions' => array(
    'topheader' => t('Top: Header'),
    'topcolumn1' => t('Top: First Column'),
    'topcolumn2' => t('Top: Second Column'),
    'middleheader' => t('Middle: Header'),
    'middlecolumn1' => t('Middle: First Column'),
    'middlecolumn2' => t('Middle: Second Column'),
    'bottomheader' => t('Top: Header'),
    'bottomcolumn1' => t('Top: First Column'),
    'bottomcolumn2' => t('Top: Second Column'),
    'footer' => t('Footer'),
  ),
);
