<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

// Plugin definition.
$plugin = array(
  'title' => t('Default site-wide template'),
  'category' => t('CocoCore'),
  'icon' => 'coco-everywhere-default.png',
  'theme' => 'coco_everywhere_default',
  'css' => 'coco-everywhere-default.css',
  'regions' => array(
    'abovecontent' => t('Above content'),
    'content' => t('Main Content'),
    'belowcontenttop' => t('Below content, first row'),
    'belowcontentmiddle1' => t('Below content, second row, first column'),
    'belowcontentmiddle2' => t('Below content, second row, second column'),
    'belowcontentbottom' => t('Below content, third row'),
    'footertop1' => t('Footer top, first column'),
    'footertop2' => t('Footer top, second column'),
    'footermiddle' => t('Footer middle'),
    'footerbottom1' => t('Footer bottom, first column'),
    'footerbottom2' => t('Footer bottom, second column'),
  ),
);
