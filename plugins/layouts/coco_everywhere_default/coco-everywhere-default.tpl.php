<?php

/**
 * @file
 * Template for the default Coco Panels Everywhere layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout.
 */

  // @todo Add 'real' template code.
?>

<div class="page-main">
  <?php if ($content['abovecontent']) : ?>
    <div class="row page-main--top">
      <?php print render($content['abovecontent']); ?>
    </div>
  <?php endif; ?>

  <?php if ($content['content']) : ?>
  <div class="row page-main--middle page-content">
    <?php print render($content['content']); ?>
  </div>
  <?php endif; ?>

  <?php if ($content['belowcontenttop'] || $content['belowcontentmiddle1'] || $content['belowcontentmiddle2'] || $content['belowcontentbottom']) : ?>
  <div class="page-main--bottom">
    <?php if ($content['belowcontenttop']) : ?>
    <div class="row">
      <?php print render($content['belowcontenttop']); ?>
    </div>
    <?php endif; ?>

    <?php if ($content['belowcontentmiddle1'] || $content['belowcontentmiddle2']) : ?>
    <div class="row">
      <div class="col-md-6">
        <?php print render($content['belowcontentmiddle1']); ?>
      </div>
      <div class="col-md-6">
        <?php print render($content['belowcontentmiddle2']); ?>
      </div>
    </div>
    <?php endif; ?>

    <?php if ($content['belowcontentbottom']) : ?>
    <div class="row">
      <?php print render($content['belowcontentbottom']); ?>
    </div>
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>

<footer class="page-footer">
  <?php if ($content['footertop1'] || $content['footertop2']) : ?>
  <div class="row">
    <div class="col-md-6">
      <?php print render($content['footertop1']); ?>
    </div>
    <div class="col-md-6">
      <?php print render($content['footertop2']); ?>
    </div>
  </div>
  <?php endif; ?>

  <?php if ($content['footermiddle']) : ?>
  <div class="row">
    <?php print render($content['footermiddle']); ?>
  </div>
  <?php endif; ?>

  <?php if ($content['footerbottom1'] || $content['footerbottom2']) : ?>
  <div class="row">
    <div class="col-md-6">
      <?php print render($content['footerbottom1']); ?>
    </div>
    <div class="col-md-6">
      <?php print render($content['footerbottom2']); ?>
    </div>
  </div>
  <?php endif; ?>
</footer>
